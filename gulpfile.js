let gulp = require('gulp');
let sass = require('gulp-sass');

// const plumber = require('gulp-plumber');
const autoprefixer = require('gulp-autoprefixer');
// const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');

// gulp.task('sass', function () {
//     return gulp.src('scss/style.scss')
//         .pipe(plumber())
//         .pipe(sourceMaps.init())
//         .pipe(sass())
//         .pipe(autoprefixer({
//         browsers: ['last 2 versions']
//         }))
//         .pipe(sourceMaps.write())
//         .pipe(gulp.dest('build/css'))
//         .pipe(browserSync.reload({stream: true}));
// });
//
// gulp.task('html', function () {
//     return gulp.src('*.html')
//         .pipe(gulp.dest('build'))
//         .pipe(browserSync.reload({stream: true}));
// });
//
// gulp.task('serve',function () {
//     browserSync.init({
//         server: "build"
//     });
//
//     gulp.watch("scss/**/*.scss", ["sass"]);
//     gulp.watch("*.html", ["html"]);
// });


gulp.task('sass', function () {
    gulp.src('./scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./css'));
});