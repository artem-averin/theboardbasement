// $('.slider').bxSlider({
//     auto: true,
//     autoControls: true,
//     stopAutoOnClick: true,
//     pager: true,
//     slideWidth: 600,
//     // slideMargin: 5,
//     minSlides: 1
// });

$(document).ready(function(){
    $('.stuff-slider').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 2,
        dots:true,
        // autoplay: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 320,
                settings: {
                    arrows: false,
                    slidesToShow: 1
                }
            }
        ]
    });
});


$(document).ready(function(){
    $('.blog__slider').slick({
        dots:true,
        // autoplay: true,
        arrows: false,
        // autoplaySpeed: 5000,
        variableWidth: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    variableWidth: false,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    variableWidth: false,
                    slidesToShow: 1
                }
            },
            // {
            //     breakpoint: 480,
            //     settings: {
            //         arrows: false,
            //         slidesToShow: 1
            //     }
            // },
            {
                breakpoint: 320,
                settings: {
                    arrows: false,
                    variableWidth: false,
                    slidesToShow: 1
                }
            }
        ]
    });
});




// $(document).ready(function(){
//     $('.owl-carousel').owlCarousel({
//         margin:10,
//         // loop:true,
//         autoWidth:true,
//         items:4,
//         responsiveClass:true
//         responsive:{
//             0:{
//                 items:1,
//                 nav:true
//             },
//             600:{
//                 items:2,
//                 nav:false
//             },
//             1024:{
//                 items:3,
//                 autoWidth:true
//             }
//         }
//     })
// });